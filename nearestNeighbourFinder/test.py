#!/usr/bin/env python

#############################################################################
import cgi
form = cgi.FieldStorage()
import cgitb
cgitb.enable()

import os, commands
import time
import sys
import string
import re

import sqlite3
import ECalDBLib
import WebTool
from WebTool import *


#############################################################################
ecalDB = ECalDBLib.ECalDBLib()
#############################################################################
runName = 'CampaignList.sh'


#############################################################################
##...Get the variables and the db
adress = ECalDBLib.dbBasePath+'/ECalDB.db'
db = sqlite3.connect(adress)
cur = db.cursor()
ecalDB.Open('sqlite://'+adress)

print "content-type: text/html"
print
print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'
print '<html xmlns="http://www.w3.org/1999/xhtml">'
print '<head>'             
print '<title>ECalCampaign</title>'
print '<meta name="author" content="Julien Morel [julien.morel@cern.ch]"/>'
print '<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico" />'
print '<link rel="Stylesheet" href="https://lar-elec-automatic-validation.web.cern.ch/lar-elec-automatic-validation/styles.css" type="text/css" />'
print '<base target="_self">'
print '</head>'      

print '<body>'              
print  '<div class="main" style="text-align:center">'

#############################################################################
#...print List
campaignList = ecalDB.Execute('select * from campaign ORDER BY date DESC')

def printTab(type):
    print '<table class="list" style="text-align:center" align="center" border="1px">'
    print '<tr>'
    print '<td> Id      </td>'
    print '<td> Date      </td>'
    print '<td> Run range </td>'
    print '<td> Type      </td>'
    print '<td> Partition </td>'
    print '<td> Toroid    </td>'
    print '<td> Solenoid  </td>'
    print '<td> DB status </td>'
    print '<td> Composite </td>'
    print '<td> DQ Flags  </td>'
    print '<td> Flags     </td>'
    print '<td colspan="2"> Validation results     </td>'
    print '<td class="rien">   </td>'
    print '</tr>'


    for campaign in campaignList:
        campaignFlag = campaign['flag']
        if 'campaignFlag_%d'%campaign['id'] in form.keys() :
            if form['campaignFlag_%d'%campaign['id']].value != campaign['flag']:
                if passWord == PW :
                    campaignFlag = form['campaignFlag_%d'%campaign['id']].value
                    ecalDB.UpdateCampaignFlag(campaign['id'], form['campaignFlag_%d'%campaign['id']].value)
                else:
                    print PW_message

        if 'comment_%d'%campaign['id'] in form.keys() :
            cc = form['comment_%d'%campaign['id']].value 
            if cc != campaign['comment']:
                if passWord == PW :
                    if cc != '' :
                        if cc == "Remove" or cc == "Delete" or cc == "remove" or cc == "delete":
                            cc=''
                        if cc != '' and re.match('[0-3][0-9]/[0-1][0-9]/[0-9][0-9]:*',cc)==None:
                            print 'Comment "%s" of campaign %d must have the format dd/mm/yy:comment' % (cc,campaign['id'])
                        else:
                                ecalDB.UpdateCampaignComment(campaign['id'], cc)
                                if campaign['comment']!='' and cc!='': campaign['comment'] = campaign['comment']+', '+cc
                                else: campaign['comment'] =cc
                else:
                    print PW_message
                    
        if campaign['ref']==0: continue
        if campaign['type']!=type and type!='ALL': continue

        nom = 'Campaign'+str(campaign['id'])+' '
        nom = nom+time.strftime('%a, %d %b %H:%M', time.localtime(campaign['date']))
        runList = ecalDB.Execute('select partition from run where campaign='+str(campaign['id']))
        part=''
        list=[]
        for i in runList:
            if not(i['partition'] in list) : list.append(str(i['partition']))
        list.sort()
        for i in list:
            part += '  '+i

        #...Get the run number range for the campaign
        listRun = ecalDB.Execute('''select id from run where campaign=%d order by id''' % (campaign['id']) )

        print '<tr  class=out1>'
        print '<td> <a href="https://lar-elec-automatic-validation.web.cern.ch/lar-elec-automatic-validation/cgi-bin/CampaignInfo.sh?Id='+str(campaign['id'])+'">'+str(campaign['id'])+'</a> </td>'
        print '<td> '+time.strftime('%a, %d %b %Y %H:%M', time.localtime(campaign['date']))+'    </td>'
        if len(listRun)>0:
            print '<td>'+str(listRun[0]['id'])+' - '+str(listRun[-1]['id'])+'</td>'
        else:
            print '<td> None (fake camp) </td>'
        print '<td> '+campaign['type']+'    </td>'
        print '<td> '+part            +'    </td>'
        print '<td> '+str(campaign['toroid_status'])+' </td>'
        print '<td> '+str(campaign['solenoid_status'])+' </td>'
        print '<td> '+campaign['db']+'  </td>'
        print '<td> '+campaign['composite']+' </td>'
        print '<td> '+campaign['dq_flag']+' </td>'
        print '<td> '
        if type == 'ALL':
            print '<SELECT class=%s name="campaignFlag_%d">' % (campaignFlag, campaign['id'])
            for color in ['RED','YELLOW','GREEN']:
                isSel = ''
                if  campaignFlag==color: isSel = 'SELECTED'
                print '<OPTION class=%s VALUE=%s  %s> %s </OPTION>' % (color, color,isSel,color)
            print '</SELECT>'
            print '</td>'
        else:
            print campaignFlag
        
        print '<td> <a href="https://lar-elec-automatic-validation.web.cern.ch/lar-elec-automatic-validation/cgi-bin/CampaignInfo.sh?Id=%d"> Summary </a> </td>' % (campaign['id'])
        print '<td> <a href="https://lar-elec-automatic-validation.web.cern.ch/lar-elec-automatic-validation/cgi-bin/runLArCalibDB.sh?campaignId=%d"> Detailed </a> </td>' % (campaign['id'])

        cmt = ''
        if campaign['comment']!='':
            cmt = campaign['comment']
            if len(cmt)>60 : cmt=cmt[0:57]+'<a href="https://lar-elec-automatic-validation.web.cern.ch/lar-elec-automatic-validation/cgi-bin/CampaignInfo.sh?Id='+str(campaign['id'])+'"> ... </a>'

        print '<td class = rien>'
        if type == 'ALL': print '<input class=out1 type="text" SIZE="5" name="comment_%d" />' % (campaign['id'])
        print cmt
        print '</td>'


        
        print '</tr>'
    print '</table>'
    
print  '<hr>'
print '<FORM class="main" method="POST" action="'+runName+'">'
if 'PassWord' in form.keys(): passWord = form['PassWord'].value
print '<h2>List of all campaigns</h2>'
print 'Password : <input class=out1 type="password" SIZE="5" name="PassWord" /> <INPUT size="3" type="submit" value="Save flags and comments"/>  '
printTab('ALL')
print '</FORM>'
ecalDB.Commit()

print  '<hr>'
print '<h2>List of weekly campaigns </h2>'
printTab('WEEKLY')

print  '<hr>'
print '<h2>List of daily campaigns </h2>'
printTab('DAILY') 

ecalDB.Close()
print '</div>'
